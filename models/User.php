<?php
namespace Mvc\Model;

require_once '../app/Model.php';
class User extends \Mvc\App\Model
{
    public function __construct()
    {
        parent::__construct();
        //echo '<br/>';
        //echo 'En el modelo User';
    }
    public function get($page)
    {
        $offset = ($page-1) * $this::PAGE_SIZE;
        $sql = "select id, name, surname, login from user limit " . $offset . "," . $this::PAGE_SIZE;
        $stmt = $this->_pdo->prepare($sql);
        $stmt->execute();
        $rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $rows;
    }

    public function getPageCount()
    {
        $sql = "select count(id) from user";
        $stmt = $this->_pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchColumn();
        return intval(ceil($result / $this::PAGE_SIZE));
    }

    public function insert($post_name, $post_surname, $post_login, $post_password)
    {
        if ($post_name != "" && $post_login != "" && $post_password != "") {
            $sql = "insert into user (name, surname, login, password) values(:name,:surname,:login,:password)";
            $stmt = $this->_pdo->prepare($sql);
            $stmt->bindValue(':name', $post_name, \PDO::PARAM_STR);
            $stmt->bindValue(':surname', $post_surname, \PDO::PARAM_STR);
            $stmt->bindValue(':login', $post_login, \PDO::PARAM_STR);
            $stmt->bindValue(':password', $post_password, \PDO::PARAM_STR);
            $stmt->execute();
        }
    }

    public function select($post_id)
    {
        
        $sql = "select id, name, surname, login from user where id  = :id";
        $stmt = $this->_pdo->prepare($sql);
        $stmt->bindValue(':id', $post_id, \PDO::PARAM_INT);
        $stmt->execute();
        $user = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $user;
    }

    public function update($post_id, $post_name, $post_surname, $post_login, $post_password)
    {
        if ($post_name != "" && $post_login != "" && $post_password != "") {
            $sql = "update user set name = :name, surname = :surname, login = :login, password = :password where id = :id";
            $stmt = $this->_pdo->prepare($sql);
            $stmt->bindValue(':id', $post_id, \PDO::PARAM_INT);
            $stmt->bindValue(':name', $post_name, \PDO::PARAM_STR);
            $stmt->bindValue(':surname', $post_surname, \PDO::PARAM_STR);
            $stmt->bindValue(':login', $post_login, \PDO::PARAM_STR);
            $stmt->bindValue(':password', $post_password, \PDO::PARAM_STR);
            $stmt->execute();
        }
    }

    public function delete($post_id)
    {
        $sql = "delete from user where id = :id";
        $stmt = $this->_pdo->prepare($sql);
        $stmt->bindValue(':id', $post_id, \PDO::PARAM_INT);
        $stmt->execute();
    }

    public function checkPassword($post_login, $post_password)
    {
        if ($post_login != "" && $post_password != "") {
            $sql = "select password from user where login = :login";
            $stmt = $this->_pdo->prepare($sql);
            $stmt->bindValue(':login', $post_login, \PDO::PARAM_STR);
            $stmt->execute();
            $password = $stmt->fetchColumn();
            if ($password==$post_password) {
                return true;
            } else {
                return false;
            }
        }
    }
}
