<?php
/*
* Clase modelo de Articulo
*/
namespace Mvc\Model;

class Articulo
{
    const DB_HOST = "localhost";
    const DB_USER = "usuario";
    const DB_PASSWORD = "usuario";
    const DB_DATABASE = "mvc";

    private $_db;
    private $rows;

    public function __construct()
    {
        $this->_db = new \mysqli($this::DB_HOST, $this::DB_USER, $this::DB_PASSWORD, $this::DB_DATABASE);
        if ($this->_db->connect_errno > 0) {
            //die("Imposible conectarse con la base de datos [" . $this->_db->connect_error . "]");
            throw new \Exception("Imposible conectarse con la base de datos.", 500);
        }
    }
    public function getAll()
    {
        $sql = "select id, nombre, precio from articulo";
        $stmt = $this->_db->stmt_init();
        if ($stmt->prepare($sql)) {
            $stmt->execute();
            $nombre = "";
            $precio = 0;
            $id = 0;
            $stmt->bind_result($id, $nombre, $precio);
            while ($stmt->fetch()) {
                $rows[] = array ('id' => $id, 'nombre' => $nombre, 'precio' => $precio);
            }
            $stmt->close();
            return $rows;
        }
        $stmt->close();
        //$result = $this->_db->query($sql);
        //if ($this->_db->errno) {
        //    die($this->_db->error);
        //}
        //while ($row = $result->fetch_assoc()) {
        //    $rows[] = $row;
        //}
        //return $rows;
        //echo '<pre>';
        //var_dump($rows);
        //echo '</pre>';
        
    }
    public function insert($post_nombre, $post_precio)
    {
        if ($post_nombre != "" && $post_precio != "") {
            $post_nombre = $this->_db->real_escape_string($post_nombre);
            //$sql = "insert into articulo(nombre, precio) values (\"" . $post_nombre . "\", " . $post_precio . ")";
            $stmt = $this->_db->stmt_init();
            $sql = "insert into articulo (nombre, precio) values(?,?)";
            if ($stmt->prepare($sql)) {
                $stmt->bind_param("sd", $nombre, $precio);
                $nombre = $post_nombre;
                $precio = $post_precio;
                $stmt->execute();
            }
            $stmt->close();
            //$result = $this->_db->query($sql);
            //if ($this->_db->errno) {
            //    die($this->_db->error);
            //}
        }
    }
    public function select($post_id)
    {
        //$sql = "select * from articulo where id = " . $id;
        //$result = $this->_db->query($sql);
        //if ($this->_db->errno) {
        //    die($this->_db->error);
        //}
        //return $result->fetch_assoc();
        $stmt = $this->_db->stmt_init();
        $sql = "select nombre, precio from articulo where id  = ?";
        if ($stmt->prepare($sql)) {
            $stmt->bind_param("i", $id);
            $id = $post_id;
            $stmt->execute();
            $nombre = "";
            $precio = 0;
            $stmt->bind_result($nombre, $precio);
            if ($stmt->fetch()) {
                $stmt->close();
                return array ('id' => $id, 'nombre' => $nombre, 'precio' => $precio);
            }
            $stmt->close();
        }
        $stmt->close();
    }
    public function update($post_id, $post_nombre, $post_precio)
    {
        if ($post_nombre != "" && $post_precio != "") {
            $post_nombre = $this->_db->real_escape_string($post_nombre);
            //$sql = "update articulo set nombre = \"" . $post_nombre . "\", precio = " . $post_precio ." where id = " . $post_id;
            $stmt = $this->_db->stmt_init();
            $sql = "update articulo set nombre = ?, precio = ? where id = ?";
            if ($stmt->prepare($sql)) {
                $stmt->bind_param("sdi", $nombre, $precio, $id);
                $nombre = $post_nombre;
                $precio = $post_precio;
                $id = $post_id;
                $stmt->execute();
            }
            $stmt->close();
            //$result = $this->_db->query($sql);
            //if ($this->_db->errno) {
            //    die($this->_db->error);
            //}
        }
    }
    public function delete($post_id)
    {
        //$sql = "delete from articulo where id = " . $id;
        //$result = $this->_db->query($sql);
        //if ($this->_db->errno) {
        //    die($this->_db->error);
        //}
        $sql = "delete from articulo where id = ?";
        $stmt = $this->_db->stmt_init();
        if ($stmt->prepare($sql)) {
            $stmt->bind_param("i", $id);
            $id = $post_id;
            $stmt->execute();
        }
        $stmt->close();
    }
}
