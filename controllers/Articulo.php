<?php
/*
* Clase controladora de Articulo
*/
namespace Mvc\Controller;

require '../models/Articulo.php';

class Articulo
{
    public $model;
    public $error;

    public function __construct()
    {
        $this->model = new \Mvc\Model\Articulo;
        //echo "Estamos en el controlador Articulo.</br>";
    }
    public function index()
    {
        //echo "Estamos en el m&eacute;todo Index.</br>";
        //Pedir lista de artículos
        $rows = $this->model->getAll();
        require '../views/articulo/index.php';
        //Generar la vista correspondiente
    }
    public function new()
    {
        //formulario de alta de datos
        require '../views/articulo/new.php';
    }
    public function insert()
    {
        //inserción de datos nuevos
        try {
            $this->model->insert($_POST["nombre"], $_POST["precio"]);
            header('Location: /articulo/');
        } catch (\Exception $e) {
            $error = new \Mvc\Controller\Error();
            $error->index($e);
        }
        
    }
    public function delete($id)
    {
        //eliminación de datos
        try {
            $this->model->delete($id);
            header('Location: /articulo/');
        } catch (\Exception $e) {
            $error = new \Mvc\Controller\Error();
            $error->index($e);
        }
    }
    public function edit($id)
    {
        //formulario de edición
        try {
            $articulo = $this->model->select($id);
            require '../views/articulo/edit.php';
        } catch (\Exception $e) {
            $error = new \Mvc\Controller\Error();
            $error->index($e);
        }
    }

    public function update($id)
    {
        //actualización de datos una vez editados
        try {
            $this->model->update($id, $_POST["nombre"], $_POST["precio"]);
            header('Location: /articulo/');
        } catch (\Exception $e) {
            $error = new \Mvc\Controller\Error();
            $error->index($e);
        }
    }
}
