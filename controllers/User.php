<?php

namespace Mvc\Controller;

require_once '../app/Controller.php';
require_once '../models/User.php';

class User extends \Mvc\App\Controller
{
    public function __construct()
    {
        parent::__construct();
        //echo '<br/>';
        //echo "En el controlador User";
        $this->_model = new \Mvc\Model\User;
        //echo '<pre>';
        //var_dump($this);
        //echo '</pre>';
    }

    public function index($page = 1)
    {
        $rows = $this->_model->get($page);
        $pages = $this->_model->getPageCount();
        require '../views/user/index.php';
    }

    public function new()
    {
        //formulario de alta de datos
        require '../views/user/new.php';
    }
    public function insert()
    {
        //inserción de datos nuevos
        try {
            if ($_POST['password'] == $_POST['password2']) {
                $password =  md5($_POST["password"]);
                $this->_model->insert($_POST["name"], $_POST["surname"], $_POST["login"], $password);
                header('Location: /user/');
            } else {
                header('Location: /user/new');
            }
        } catch (\Exception $e) {
            $error = new \Mvc\Controller\Error();
            $error->index($e);
        }
        
    }
    public function delete($id)
    {
        //eliminación de datos
        try {
            $this->_model->delete($id);
            header('Location: /user/');
        } catch (\Exception $e) {
            $error = new \Mvc\Controller\Error();
            $error->index($e);
        }
    }
    public function edit($id)
    {
        //formulario de edición
        try {
            $user = $this->_model->select($id);
            require '../views/user/edit.php';
        } catch (\Exception $e) {
            $error = new \Mvc\Controller\Error();
            $error->index($e);
        }
    }

    public function update($id)
    {
        //actualización de datos una vez editados
        try {
            if ($_POST['password'] == $_POST['password2']) {
                $password = md5($_POST["password"]);
                $this->_model->update($id, $_POST["name"], $_POST["surname"], $_POST["login"], $password);
                header('Location: /user/');
            } else {
                header('Location: /user/edit/' . $id);
            }
        } catch (\Exception $e) {
            $error = new \Mvc\Controller\Error();
            $error->index($e);
        }
    }
}
