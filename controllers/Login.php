<?php

namespace Mvc\Controller;

require_once '../app/Controller.php';
require_once '../models/User.php';

class Login extends \Mvc\App\Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->_model = new \Mvc\Model\User;
    }

    public function index()
    {
        require '../views/login/index.php';
    }

    public function run()
    {
        $password = md5($_POST['password']);
        $correct = $this->_model->checkPassword($_POST['login'], $password);
        echo $correct;
        if ($correct==true) {
            $_SESSION['username'] = $_POST['login'];
            header('Location: /');
        } else {
            header('Location: /login');
        }
    }

    public function out()
    {
        session_unset();
        header('Location: /');
    }
}
