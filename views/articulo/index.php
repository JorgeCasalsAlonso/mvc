<?php
    require '../views/header.php';
?>
<div id="content">
    <h1>Lista de art&iacute;culos</h1>
    <p><a href=<?php echo '/articulo/new/'?>>Nuevo art&iacute;culo</a></p>
    <?php if (array_count_values($rows)==0) : ?>
    	<p>No hay elementos que mostrar.</p>
    <?php else : ?>
    	<table>
	    	<?php foreach ($rows as $i => $row) : ?>
	    		<?php if ($i == 0) : ?>
	    			<thead>
				    	<tr>
					    	<?php foreach ($row as $key => $value) : ?>
					    		<th><?php echo ucfirst($key) ?></th>
					    	<?php endforeach ?>
					    	<th colspan="2">Acciones</th>
				    	</tr>
			    	</thead>
	    		<?php endif ?>
	    		<tr>
	    			<?php foreach ($row as $key => $value) : ?>
	    				<td><?php echo $value ?></td>
	    			<?php endforeach ?>
					<td><a href=<?php echo '/articulo/edit/' . $row['id'] ?>>Editar</a></td>
	    			<td><a href=<?php echo '/articulo/delete/' . $row['id'] ?>>Borrar</a></td>
	    		</tr>
    		<?php endforeach ?>
    	</table>
    <?php endif ?>
</div>

<?php
    require '../views/footer.php';
?>
