<!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8">
	    <link rel="stylesheet" type="text/css" href="/css/default.css">
	    <title>Framework MVC. Curso 2016/2017</title>
	</head>
	<body>
	<div id="header">
		<div id="title">Framework MVC. Curso 2016/2017</div>
		<?php if ($_SESSION['username'] != '') : ?>
			<div id="user">Usuario: <?php echo $_SESSION['username'] ?></div>
		<?php endif ?>
		<div>
			<a href="/index">Inicio</a>
		    <a href="/articulo">Art&iacute;culos</a>
		    <a href="/user">Usuarios</a>
		    <a href="/help">Ayuda</a>
		    <?php if ($_SESSION['username'] == '') : ?>
				<a id="session"'" href="/login">Iniciar sesi&oacute;n</a>
			<?php else : ?>
				<a id="session" href="/login/out">Cerrar sesi&oacute;n</a>
			<?php endif ?>
		</div>
			
	</div>
