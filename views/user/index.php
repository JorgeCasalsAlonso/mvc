<?php
    require '../views/header.php';
?>
<div id="content">
    <h1>Lista de usuarios</h1>
    <p><a href=<?php echo '/user/new/'?>>Nuevo usuario</a></p>
    <?php if (array_count_values($rows)==0) : ?>
    	<p>No hay elementos que mostrar.</p>
    <?php else : ?>
    	<table>
	    	<?php foreach ($rows as $i => $row) : ?>
	    		<?php if ($i == 0) : ?>
	    			<thead>
				    	<tr>
					    	<?php foreach ($row as $key => $value) : ?>
					    		<th><?php echo ucfirst($key) ?></th>
					    	<?php endforeach ?>
					    	<th colspan="2">Acciones</th>
				    	</tr>
			    	</thead>
	    		<?php endif ?>
	    		<tr>
	    			<?php foreach ($row as $key => $value) : ?>
	    				<td><?php echo $value ?></td>
	    			<?php endforeach ?>
					<td><a href=<?php echo '/user/edit/' . $row['id'] ?>>Editar</a></td>
	    			<td><a href=<?php echo '/user/delete/' . $row['id'] ?>>Borrar</a></td>
	    		</tr>
    		<?php endforeach ?>
    	</table>
    	<a href=<?php echo "/user/index/1" ?>>&laquo</a>&nbsp;
    	<?php if ($page>1) : ?>
    		<a href=<?php echo "/user/index/" . ($page-1) ?>>&lt;</a>&nbsp;
    	<?php endif ?>
    	<?php for ($i = 1; $i<=$pages; $i++) : ?>
    		<a href=<?php echo "/user/index/" . $i ?>><?php echo $i ?></a>&nbsp;
    	<?php endfor ?>
    	<?php if ($page<$pages) : ?>
    		<a href=<?php echo "/user/index/" . ($page+1) ?>>&gt;</a>
    	<?php endif ?>
    	<a href=<?php echo "/user/index/" . ($pages) ?>>&raquo</a>&nbsp;
    <?php endif ?>
</div>

<?php
    require '../views/footer.php';
?>
