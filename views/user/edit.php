<?php
    require '../views/header.php';
?>
<div id="content">
    <h1>Editar usuario</h1>
    <form action=<?php echo "/user/update/" . $user['id']?> method="post">
    	<table>
		    <tr>
		    	<td>Nombre:</td>
				<td><input type="text" name="name" value=<?php echo $user['name'] ?>></td>
		    </tr>
		    <tr>
		    	<td>Apellido:</td>
				<td><input type="text" name="surname" value=<?php echo $user['surname'] ?>></td>
		    </tr>
		    <tr>
		    	<td>Login:</td>
				<td><input type="text" name="login" value=<?php echo $user['login'] ?>></td>
		    </tr>
		    <tr/>
	    	<tr>
		    	<td>Contrase&ntilde;a:</td>
				<td><input type="password" name="password"></td>
		    </tr>
		    <tr>
		    	<td>Repetir contrase&ntilde;a:</td>
				<td><input type="password" name="password2"></td>
		    </tr>
	    </table>
	    <input type="submit" value="Modificar">
    </form>
</div>

<?php
    require '../views/footer.php';
?>
