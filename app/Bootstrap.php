<?php
/**
* Clase Bootstrap
* Controlador frontal de la aplicación
*/
namespace Mvc\App;

require '../controllers/Error.php';
class Bootstrap
{
    public function __construct()
    {
//        echo "Dentro de bootstrap, __construct";
        session_start();
        $url =  $this->_getUrl();
//        echo "<pre>";
//        var_dump($url);
//        echo "</pre>";
//        echo "Controlador: $url[0] <br>";
//        echo "Metodo: $url[1]";
        //cargar controlador
        try {
            $this->_load($url);
        } catch (\Exception $e) {
            $error = new \Mvc\Controller\Error();
            $error->index($e);
        }
    }


    private function _getUrl()
    {
        if (isset($_GET['url'])) {
            $url = rtrim($_GET['url'], "/");
            $url = explode("/", $url);
        } else {
            $url[] = 'index';
            $url[] = 'index';
        }
        if (count($url) == 1) {
            $url[] = 'index';
        }
        return $url;
    }

    private function _load($url)
    {
        $controllerName =  ucfirst(array_shift($url));
        $cambiado = false;
        if ($controllerName != "Index" and $controllerName != "Help" and $controllerName != "Error" and $controllerName != "Login" and $_SESSION['username']=='') {
            $controllerName = 'Login';
            $cambiado = true;
        }
        if (file_exists('../controllers/' . $controllerName . '.php')) {
            require_once '../controllers/' . $controllerName . '.php';
            if ($cambiado == true) {
                $method = 'index';
            } else {
                $method = array_shift($url); //tomo el método
            }
            $controllerName = '\\Mvc\\Controller\\' . $controllerName;
            $controller = new  $controllerName; //creo controlador
            if (method_exists($controller, $method)) {
                call_user_func_array(array($controller, $method), $url);
            } else {
                //die("Método $method no encontrado");
                throw new \Exception("Método $method no encontrado", 404);
            }
        } else {
            //die("Controlador $controllerName no encontrado");
            throw new \Exception("Controlador $controllerName no encontrado", 404);
        }
    }
}
