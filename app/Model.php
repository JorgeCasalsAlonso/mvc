<?php

namespace Mvc\App;

class Model
{
    protected $_pdo;
    const PAGE_SIZE = 3;
    
    public function __construct()
    {
        //echo "En el padre de los modelos";
        $this->_pdo = new \PDO('mysql:host=localhost;dbname=mvc', 'root', 'root');
    }
}
